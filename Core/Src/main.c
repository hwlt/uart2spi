/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#define READWRITE_CMD                     ((uint8_t)0x80)
#define LIS302DL_WHO_AM_I_ADDR                  0x0F
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
#define BLIINK_DELAY_MS  500
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;

UART_HandleTypeDef huart2;

/* Definitions for usartCLITask */
osThreadId_t usartCLITaskHandle;
const osThreadAttr_t usartCLITask_attributes = {
  .name = "usartCLITask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for spiContollerTas */

osThreadId_t spiContollerTasHandle;
const osThreadAttr_t spiContollerTas_attributes = {
  .name = "spiContollerTas",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityLow,
};

#define SPI_DATA_WRITE_MAX_LEN         14
#define SPI_DATA_READ_MAX_LEN          14

typedef struct {                                // object data type

  uint8_t id;
  uint8_t dataWriteLen;
  uint8_t dataWrite[SPI_DATA_WRITE_MAX_LEN];
  uint8_t dataReadLen;

} spi_controller_msg_t;

static spi_controller_msg_t spi_msg;


typedef enum {
	SPI_MSG_ID_READ,
	SPI_MSG_ID_WRITE,
	SPI_MSG_ID_WRITE_READ

} spi_contoller_msg_id_t;

/* Definitions for blinkyTask */
osThreadId_t blinkyTaskHandle;
const osThreadAttr_t blinkyTask_attributes = {
  .name = "blinkyTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityLow,
};
/* Definitions for uartCLIQueue */
osMessageQueueId_t uartCLIQueueHandle;
const osMessageQueueAttr_t uartCLIQueue_attributes = {
  .name = "uartCLIQueue"
};
/* Definitions for spiController */
osMessageQueueId_t spiControllerHandle;
const osMessageQueueAttr_t spiController_attributes = {
  .name = "spiController"
};
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI1_Init(void);
static void MX_USART2_UART_Init(void);
void startUsartCLITask(void *argument);
void startSpiContollerTask(void *argument);
void startBlinkyTask(void *argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* creation of uartCLIQueue */
  uartCLIQueueHandle = osMessageQueueNew (4, 128, &uartCLIQueue_attributes);

  /* creation of spiController */
  spiControllerHandle = osMessageQueueNew (4, 17, &spiController_attributes);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of usartCLITask */
  usartCLITaskHandle = osThreadNew(startUsartCLITask, NULL, &usartCLITask_attributes);

  /* creation of spiContollerTas */
  spiContollerTasHandle = osThreadNew(startSpiContollerTask, NULL, &spiContollerTas_attributes);

  /* creation of blinkyTask */
  blinkyTaskHandle = osThreadNew(startBlinkyTask, NULL, &blinkyTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(CS_I2C_SPI_GPIO_Port, CS_I2C_SPI_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, LD4_Pin|LD3_Pin|LD5_Pin|LD6_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : CS_I2C_SPI_Pin */
  GPIO_InitStruct.Pin = CS_I2C_SPI_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(CS_I2C_SPI_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LD4_Pin LD3_Pin LD5_Pin LD6_Pin */
  GPIO_InitStruct.Pin = LD4_Pin|LD3_Pin|LD5_Pin|LD6_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : MEMS_INT1_Pin */
  GPIO_InitStruct.Pin = MEMS_INT1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(MEMS_INT1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : MEMS_INT2_Pin */
  GPIO_InitStruct.Pin = MEMS_INT2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(MEMS_INT2_GPIO_Port, &GPIO_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

int _write(int fd, char * ptr, int len)
{
  HAL_UART_Transmit(&huart2, (uint8_t *) ptr, len, HAL_MAX_DELAY);
  return len;
}


/* USER CODE END 4 */

/* USER CODE BEGIN Header_startUsartCLITask */
#define MAX_CMD_LENGTH     128
static uint8_t  tmpChar;
static uint8_t uart_cmd_len = 0;
static uint8_t uart_cmd_buf[MAX_CMD_LENGTH];

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

  // If the buffer overloaded and variable tmp isn't a special symbol
  if(uart_cmd_len == MAX_CMD_LENGTH && tmpChar != '\177' && tmpChar != '\r') {
	  HAL_UART_Receive_IT(&huart2, &tmpChar, 1);
	  return;
  }

  switch(tmpChar){
  case '\r':
	  osMessageQueuePut(uartCLIQueueHandle, uart_cmd_buf, osPriorityNormal, 0U);
	  memset(uart_cmd_buf, 0, sizeof(uart_cmd_buf));
	  uart_cmd_len = 0;
	  //printf("\n\r>> ");
	  break;
  case  '\177':

	  if(uart_cmd_len > 0) uart_cmd_len--;
	  break;
  default:

	  uart_cmd_buf[uart_cmd_len] = tmpChar;
	  uart_cmd_len++;

	  break;

  }

    HAL_UART_Transmit(&huart2, &tmpChar, 1, 10);
    HAL_UART_Receive_IT(&huart2, &tmpChar, 1);

}

/**
  * @brief  Function implementing the usartCLITask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_startUsartCLITask */
void startUsartCLITask(void *argument)
{
  /* USER CODE BEGIN 5 */
  /* Infinite loop */
  osStatus_t status;
  char cmd[MAX_CMD_LENGTH]= { 0 };

  memset(uart_cmd_buf, 0, sizeof(uart_cmd_buf));
  HAL_UART_Receive_IT( &huart2, &tmpChar, 1);

  printf("\r\nEnter command:\n");

  for(;;)
  {
	  status = osMessageQueueGet(uartCLIQueueHandle, cmd, NULL, 10);
	  if (status != osOK) continue;

	  //printf("\n\rIncoming command: %s \r\n", cmd);
	  printf("\n\r");

	  if(strncmp("echo", cmd, 4 ) == NULL ){

    	  printf("%s\r\n", &cmd[5]);

      }else if( strncmp("spi_wr", cmd, 6 ) == NULL ){


    	  spi_msg.id			= SPI_MSG_ID_WRITE_READ;
          spi_msg.dataWrite[0] 	= LIS302DL_WHO_AM_I_ADDR |(uint8_t)READWRITE_CMD;
          spi_msg.dataWriteLen	= 1;
          spi_msg.dataReadLen 	= 1;

          osMessageQueuePut(spiControllerHandle, &spi_msg, osPriorityNormal, 0U);

          /*
          uint8_t ReadAddr = LIS302DL_WHO_AM_I_ADDR |(uint8_t)READWRITE_CMD;
    	  uint8_t chipID = 0;


    	  HAL_GPIO_WritePin(CS_I2C_SPI_GPIO_Port, CS_I2C_SPI_Pin, GPIO_PIN_RESET );

    	  HAL_SPI_Transmit(&hspi1,&ReadAddr,1,10);
    	  HAL_SPI_Receive(&hspi1,&chipID,1,10);

    	  HAL_GPIO_WritePin(CS_I2C_SPI_GPIO_Port, CS_I2C_SPI_Pin, GPIO_PIN_SET );
    	  printf("LIS302DL Chip ID: %x \r\n", chipID);
		  */
          continue;

      }else{

    	  printf("Unsupported command. Try again.\r\n");

      }

	  printf("Enter command:\n");
  }

  /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_startSpiContollerTask */


/**
* @brief Function implementing the spiContollerTas thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_startSpiContollerTask */
void startSpiContollerTask(void *argument)
{
  /* USER CODE BEGIN startSpiContollerTask */
  osStatus_t status;
  uint8_t readData[SPI_DATA_READ_MAX_LEN] = {0};
  char uart_cli_msg[128] = {0};

  /* Infinite loop */

  for(;;)
  {
	status = osMessageQueueGet(spiControllerHandle, &spi_msg, NULL, 10);
	if (status != osOK) continue;

	switch(spi_msg.id){
    case SPI_MSG_ID_READ:

    	break;
    case SPI_MSG_ID_WRITE:

    	break;
    case SPI_MSG_ID_WRITE_READ:


    	 HAL_GPIO_WritePin(CS_I2C_SPI_GPIO_Port, CS_I2C_SPI_Pin, GPIO_PIN_RESET );

    	 HAL_SPI_Transmit(&hspi1, spi_msg.dataWrite, spi_msg.dataWriteLen, 10);
    	 HAL_SPI_Receive(&hspi1, readData, spi_msg.dataReadLen, 10);

    	 HAL_GPIO_WritePin(CS_I2C_SPI_GPIO_Port, CS_I2C_SPI_Pin, GPIO_PIN_SET );

    	 snprintf(uart_cli_msg, sizeof(uart_cli_msg), "echo read %d bytes over SPI: ",  spi_msg.dataReadLen);

    	 for (int i = 0; i < spi_msg.dataReadLen; i++){
    		 snprintf(uart_cli_msg, sizeof(uart_cli_msg), "%s 0x%02x", uart_cli_msg, readData[i]);
    	 }

    	 osMessageQueuePut(uartCLIQueueHandle, &uart_cli_msg, osPriorityNormal, 0U);

    	 //printf("LIS302DL Chip ID: %x \r\n", readData[0]);

    	break;
    default:

    	break;
    }

    osDelay(1);
  }

  /* USER CODE END startSpiContollerTask */
}

/* USER CODE BEGIN Header_startBlinkyTask */
/**
* @brief Function implementing the blinkyTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_startBlinkyTask */
void startBlinkyTask(void *argument)
{
  /* USER CODE BEGIN startBlinkyTask */
  /* Infinite loop */
  for(;;)
  {
	  HAL_GPIO_WritePin(GPIOD, LD4_Pin|LD3_Pin|LD5_Pin|LD6_Pin, GPIO_PIN_SET);
	  osDelay(BLIINK_DELAY_MS);
	  HAL_GPIO_WritePin(GPIOD, LD4_Pin|LD3_Pin|LD5_Pin|LD6_Pin, GPIO_PIN_RESET);
	  osDelay(BLIINK_DELAY_MS);

  }
  /* USER CODE END startBlinkyTask */
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
